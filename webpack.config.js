module.exports = {
  mode: "development",
  entry: "./src/js/index.js",
  module: {
    rules: [{
      test: /\.s?css$/,
      use: [
        "style-loader", // creates style nodes from JS strings
        "css-loader", // translates CSS into CommonJS
        "sass-loader" // compiles Sass to CSS
      ]
    }]
  }
}